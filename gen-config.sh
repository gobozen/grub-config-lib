#!/bin/bash
# (re)generate  grub-generated-config.cfg
# when packaged this will be a (sym)link to /usr/share/grub/$( basename $0 )



# if user has set $timeout, but not $menu_timeout or $quick_timeout, then
# use $timeout as one of $menu_ or $quick_
# store and unset $default in $default_entry
function init_compat_timeout {
  
  if  [ "$default" ]
  then  set default_entry=$default ; unset default
  fi
  if  [ ! "$timeout" ]  ; then  return  ; fi

  if  [ ! "$menu_timeout"  -a  ! "$quick_timeout" ]
  then
    if  [ 0 -le $timeout  -a  ("$timeout_style" == hidden  -o  "$timeout_style" == countdown) ]
      # $timeout is set (not negative) and style is hidden/countdown  -> set quick_
    then  quick_timeout=$timeout ; quick_timeout_style=$timeout_style  ; unset  timeout timeout_style
      # otherwise  -> set menu_
    else  menu_timeout=$timeout  ; unset  timeout timeout_style
    fi
  elif  [ ! "$menu_timeout"  -a  ( $quick_timeout -lt $timeout ) ]
    # no $menu_ -> yes $quick_, if $quick_ < $timeout  -> set menu_
  then  menu_timeout=$timeout  ; unset  timeout timeout_style
  elif  [ ! "$quick_timeout"  -a  ( 0 -le $timeout  -a  $timeout -lt $menu_timeout ) ]
    # no $quick_ -> yes $menu_, if 0 < $timeout < $menu_  -> set quick_
  then  quick_timeout=$timeout ; quick_timeout_style=$timeout_style  ; unset  timeout timeout_style
  fi
  
  if  [ "$timeout" ]
  then  set default_timeout=$timeout ; unset timeout
  fi
  
}	# init_compat_timeout


