### Debian/Ubuntu official packages: a _lot_ of patches:
- https://anonscm.debian.org/cgit/pkg-grub/grub.git/tree/debian/patches /mkconfig*

- quick_boot
- recordfail
- recovery
  - etc.
- https://anonscm.debian.org/cgit/pkg-grub/grub.git/tree/debian/grub.d /05_debian_theme
- $bootdrive: http://bazaar.launchpad.net/~cyphermox/ubuntu/wily/grub2/lp1097570/view/head:/debian/patches/bootdrive.patch
- http://bazaar.launchpad.net/~cyphermox/ubuntu/wily/grub2/lp1097570/files/head:/debian/grub-extras/915resolution/


quick_boot.patch
- $quick_timeout=3 by default
  - effective after booting the (saved) default
  - after installing grub boot the same entry two times:
    at first it's saved as default, the second time quick_boot is enabled
  - enter the menu by pressing ESC or holding shift/ctrl/alt modifier keys (default setting)
  - boot a different entry to disable quick_boot and use $menu_timeout
  - boot the same entry to reenable quick_boot
- $menu_timeout=30 by default
  - after installing grub or choosing a different entry to boot
- $bootfailed_timeout=$menu_timeout by default
  - if recordfail=1 is not reset (by init script grub-common) then the menu is entered
  - headless servers wait indefinitely in boot menu after a boot-time powerloss
    - this is a default and can be changed (after it happens to the sysadmin)

recordfail
- is it meant to avoid repeated failed reboots?
- todo: detect quick reboots - save boot timestamp, ignore recordfail if time since last boot > 2-5 minutes
  - there may be an issue rebooting the os every few minutes
  - headless servers waiting in the bootmanager are a management nightmare:
    http://ubuntuforums.org/archive/index.php/t-1703465.html
  - spending those minutes in the os gives a chance for an admin to log in and fix the issue
- showing menu with $bootfailed_timeout/$menu_timeout may be enough
- what switches off $recordfail ?

gfxpayload_dynamic.patch
vt_handoff
- https://wiki.ubuntu.com/FoundationsTeam/Grub2BootFramebuffer

quiet_boot (maybe_quiet.patch)


### Launchpad (Ubuntu), not in official packages

$bootdrive: hd0, without partition number
- http://bazaar.launchpad.net/~cyphermox/ubuntu/wily/grub2/lp1097570/view/head:/debian/patches/bootdrive.patch
- not included in debian, ubuntu packages


### Arch:
- https://github.com/manjaro/packages-core/tree/master/grub

grub.d/31_hold_shift
- show menu only if shift is pressed
  - https://wiki.archlinux.org/index.php/GRUB/Tips_and_tricks#Hide_GRUB_unless_the_Shift_key_is_held_down
  - https://github.com/hobarrera/grub-holdshift/blob/optional/31_hold_shift
  - https://bbs.archlinux.org/viewtopic.php?id=165900
- -> set show_menu_mod_keys='--shift'  -> grub-lib-button.cfg/init_menu_mod_keys()
  - by default any of shift/ctrl/alt will show the menu
- GRUB_FORCE_HIDDEN_MENU=true  ->  if keystatus (console supports modifier keys) then set quick_timeout=0 , else 3

- LINUX_ROOT_DEVICE="/dev/disk/by-uuid/${GRUB_DEVICE_UUID}"
  - http://pkgbuild.com/git/aur-mirror.git/tree/burg-bzr/arch-burg.patch


