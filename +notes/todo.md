

grub.d/... script for grub-generated-*
- entries
  - local: distrib-specific
  - neighbour
    - use *-entries-${partX}-*.cfg  in-place
    - os-prober: skim grub.cfg for entries
- config
  - template with /etc/default/grub vars
  - cat (dump) necessary optional lib functions

grubconfig:
- compare 00_header: sab vs man vs elem/ubu/deb
- install $prefix/...:  backgrounds, fonts?, layouts
- generate:
  - available_langs ($prefix/locale)
  - available_keymaps ($prefix/layouts)
  - available_backgrounds ($prefix/backgrounds)
  - available_themes ($prefix/themes)

